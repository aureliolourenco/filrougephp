<title>Chily - Pepper</title>
<?php
require_once '../src/Singleton.php';
require_once '../src/envReader.php';
require_once __DIR__. '/../src/Model/DAOCountry.php';
try {
  $cnx = Singleton::getInstance(new PDO(SingletonIni::env('connect'). ":host". SingletonIni::env('host'). ";dbname:". SingletonIni::env('database'), SingletonIni::env('login'), SingletonIni::env('password') ));
  $daocountry = new DAOCountry($cnx);
} catch (\Throwable $th) {
    echo $th->getMessage();
}

