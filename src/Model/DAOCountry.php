<?php

/**
 * undocumented class
 */
require_once __DIR__. '/../Controller/Country.php';

class DAOCountry
{

  private $cnx;

  public function __construct($cnx)
  {
    $this->cnx = $cnx;
  }

  public function findAll()
  {
    $req = "SELECT * FROM country";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->execute();
    return $preparedStatement->fetchObject("Country");
  }

  public function find($id)
  {
    $req = "SELECT * FROM country where Country_Id = :id";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->bindParam("id", $id);
    $preparedStatement->execute();
    return $preparedStatement->fetchObject("Country");
  }

  public function save(Country $country)
  {
    $req = "INSERT INTO country (Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2, Image1, Image2, ) VALUES
    (:code, :name:, :continent, :region, :surfaceArea, :IndepYear, :Population, :LifeExpectancy, :GNP, :GNPOld, :localName, :GovernmentForm, :HeadOfState, :Capital, :Code2, :Image1, :Image2 )";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->bindParam("code", $country->Code);
    $preparedStatement->bindParam("name", $country->Name);
    $preparedStatement->bindParam("continent", $country->Continent);
    $preparedStatement->bindParam("region", $country->Region);
    $preparedStatement->bindParam("surfaceArea", $country->IndepYear);
    $preparedStatement->bindParam("Population", $country->Population);
    $preparedStatement->bindParam("LifeExpectancy", $country->LifeExpectancy);
    $preparedStatement->bindParam("GNP", $country->GNP);
    $preparedStatement->bindParam("GNPOld", $country->GNPOld);
    $preparedStatement->bindParam("localName", $country->LocalName);
    $preparedStatement->bindParam("GovernmentForm", $country->GovernmentForm);
    $preparedStatement->bindParam("HeadOfState", $country->HeadOfState);
    $preparedStatement->bindParam("Capital", $country->Capital);
    $preparedStatement->bindParam("Code2", $country->Code2);
    $preparedStatement->bindParam("Image1", $country->Image1);
    $preparedStatement->bindParam("Image2", $country->Image2);
    $preparedStatement->execute();
  }

  public function update(Country $country)
  {
    $req = "UPDATE country SET Code = :Code, Name = :Name, Continent = :Continent, Region = :Region, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy, GNP = :GNP, GNPOld = :GNPOld, LocalName = :localName, GovernmentForm = :GovernmentForm, HeadOfState = :HeadOfState, Capital = :Capital, Code2 = :Code2, Image1 = :Image1, Image2 = :Image2 WHERE Country_Id = :id;";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->bindParam("id", $country->Country_Id);
    $preparedStatement->bindParam("Code", $country->Code);
    $preparedStatement->bindParam("Name", $country->Name);
    $preparedStatement->bindParam("Continent", $country->Continent);
    $preparedStatement->bindParam("Region", $country->Region);
    $preparedStatement->bindParam("SurfaceArea", $country->SurfaceArea);
    $preparedStatement->bindParam("IndepYear", $country->IndepYear);
    $preparedStatement->bindParam("Population", $country->Population);
    $preparedStatement->bindParam("LifeExpectancy", $country->LifeExpectancy);
    $preparedStatement->bindParam("GNP", $country->GNP);
    $preparedStatement->bindParam("GNPOld", $country->GNPOld);
    $preparedStatement->bindParam("localName", $country->LocalName);
    $preparedStatement->bindParam("GovernmentForm", $country->GovernmentForm);
    $preparedStatement->bindParam("HeadOfState", $country->HeadOfState);
    $preparedStatement->bindParam("Capital", $country->Capital);
    $preparedStatement->bindParam("Code2", $country->Code2);
    $preparedStatement->bindParam("Image1", $country->Image1);
    $preparedStatement->bindParam("Image2", $country->Image2);
    $preparedStatement->execute();
  }

  public function remove($id)
  {
    $req = "DELETE FROM country WHERE Country_Id = :id";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->bindParam("id", $id);
    return $preparedStatement->execute();
  }

  public function count()
  {
    $req = "SELECT COUNT(*) FROM country";
    $preparedStatement = $this->cnx->prepare($req);
    $preparedStatement->execute();
    return $preparedStatement->fetch()[0];
  }
}
