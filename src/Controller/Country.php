<?php
class Country {
  public $Country_Id;
  public $Code;
  public $Name;
  public $Continent;
  public $Region;
  public $SurfaceArea;
  public $IndepYear;
  public $Population;
  public $LifeExpectancy;
  public $GNP;
  public $GNPOld;
  public $LocalName;
  public $GovernmentForm;
  public $HeadOfState;
  public $Capital;
  public $Code2;
  public $Image1;
  public $Image2;
  
  public function __construct($params=null) {
    if (!($params == null)) {
    $this->Country_Id = $params['Country_Id'];
    $this->Code = $params['Code'];
    $this->Name = $params['Name'];
    $this->Continent =  $params['Continent'];
    $this->Region = $params['Region'];
    $this->SurfaceArea = $params['SurfaceArea'];
    $this->IndepYear = $params['IndepYear'];
    $this->Population = $params['Population'];
    $this->LifeExpectancy = $params['LifeExpectancy'];
    $this->GNP = $params['GNP'];
    $this->GNPOld = $params['GNPOld'];
    $this->LocalName = $params['LocalName'];
    $this->GovernmentForm =$params['GovernmentForm'];
    $this->HeadOfState = $params['HeadOfState'];
    $this->Capital = $params['Capital'];
    $this->Code2 = $params['Code2'];
    $this->Image1 = $params['Image1'];
    $this->Image1 = $params['Image2'];
    }
  }
}