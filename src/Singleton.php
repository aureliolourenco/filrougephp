<?php
class Singleton {
  public $cnx;
  private $instance;
  private function __construct($cnx) {
    $this->cnx = $cnx;
  }

  public function getInstance($cnx) {
    if (is_null(self::$instance)) {
      self::$instance = new Singleton($cnx);
    }
    return self::$instance;
  }
}