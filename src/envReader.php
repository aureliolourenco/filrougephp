<?php
class SingletonIni {

  private $host;
  private $connect;
  private $port;
  private $database;
  private $login;
  private $password;
  private static $instance;

  public function __construct($fichier) {
    $config = parse_ini_file($fichier);
    $this->connect = $config['DB_CONNECTION'];
    $this->host = $config['DB_HOST'];  
    $this->port = $config['DB_PORT'];
    $this->database = $config['DB_DATABASE'];
    $this->login = $config['DB_USERNAME'];
    $this->password = $config['DB_PASSWORD'];
  }

  //reçois en entrée le nom de la variable à récupérer, retourne le contenu de la variable 
  public static function env($var) {
    if (is_null(self::$instance)) {
      self::$instance = new SingletonIni(__DIR__. '/../.env');
    }
    return self::$instance->$var;
  }

}



